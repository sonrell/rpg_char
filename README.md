A program to create RPG-characters.
The program can create 3 types of characters: Warrior, Mage, and Ranger  (called "hero" in the code).
It is possible to extend the program to design new ones.
There are 3 built in weapons which heros can equip as well as three types of armor. Both add bonus stats to your hero when he wears them. 
Look in Main.java for examples. 
