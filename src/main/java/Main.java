import armor.Body;
import armor.Legs;
import armor.SlotArmor;
import armor.clothArmor.ClothArmor;
import armor.plateArmor.PlateArmor;
import hero.mage.Mage;
import hero.ranger.Ranger;
import hero.warrior.Warrior;
import weapon.meleeWeapon.MeleeWeapon;
import weapon.rangerWeapon.RangerWeapon;

import static hero.equip.EquipArmor.equipArmor;
import static hero.equip.EquipArmor.removeArmor;
import static hero.equip.EquipWeapon.equipWeapon;
import static hero.equip.EquipWeapon.removeWeapon;
import static print.PrintArmor.printArmor;
import static print.PrintHero.printHero;
import static print.PrintWeapon.printWeapon;


public class Main {

    static int returnStartXpByLevel(int toLvl) {
        double calc=0;
        if (toLvl==1){
            return 0;
        }
        for (int i = 2; i <=toLvl ; i++) {
            calc+=100*Math.pow(1.1,i-2);
        }

        return (int)calc;

    }

    public static void main(String[] args) {


        Warrior Goliat=new Warrior(9);
        Mage David=new Mage(10);
        Ranger Timmy=new Ranger(12);

        MeleeWeapon Gaoe=new MeleeWeapon(5,"Great Axe of Exiled");
        RangerWeapon Lbotw= new RangerWeapon(10, "Long Bow of the Lone Wolf");


        SlotArmor leg=new Legs();
        SlotArmor body=new Body();

        ClothArmor clotm=new ClothArmor(10, leg, "Cloth Leggings of the Magi");
        PlateArmor pcotj=new PlateArmor(10,body, "Plate Chest of the Juggernaut");

        System.out.println("Same heros as in assignment example:");
        printHero(Goliat);
        printHero(David);
        printHero(Timmy);

        System.out.println("Same weapon as in assignment example");
        printWeapon(Gaoe);
        printWeapon(Lbotw);

        System.out.println("Same Armor as in assignment:");
        printArmor(clotm);
        printArmor(pcotj);

        pcotj.setArmorLevel(5);


        System.out.println("Equipping like in the example and try to equip a weapon lvl which is too high");
        equipArmor(pcotj, Goliat);
        equipWeapon(Gaoe, Goliat);
        equipWeapon(Lbotw,Goliat);
        printHero(Goliat);

        System.out.println("Removing equipment");
        removeArmor(Goliat);
        removeWeapon(Goliat);
        printHero(Goliat);




        System.out.println("xp set to 2000");
        Goliat.setXp(2000);
        printHero(Goliat);

        System.out.println("lvl set to 4");
        Goliat.setLvl(4);
        printHero(Goliat);

        System.out.println("xp set to 300");
        Goliat.setXp(300);
        printHero(Goliat);

        System.out.println("Set to lvl 1");
        Goliat.setLvl(1);
        printHero(Goliat);



        System.out.println("Equipping new weapon after weapon lvl is set to 1");
        Gaoe.setLvl(1);
        equipWeapon(Gaoe,Goliat);
        printHero(Goliat);










    }
}
