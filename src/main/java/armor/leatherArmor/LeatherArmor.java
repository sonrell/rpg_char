package armor.leatherArmor;

import armor.Armor;
import armor.ArmorType;
import armor.SlotArmor;

public class LeatherArmor extends Armor {

    // Static properties for Leather
    static final int baseHealth=20;
    static final int baseStrength=1;
    static final int baseDexterity=3;
    static final int baseIntelligence=0;

    static final int scaleHealthBy=8;
    static final int scaleStrengthBy=1;
    static final int scaleDexterityBy=2;
    static final int scaleIntelligenceBy=0;

    public LeatherArmor(int lvl, SlotArmor slotArmor, String name) {
        super(lvl, slotArmor, name);
    }


    //Overidden methods from interface and super
    @Override
    public void setArmorType() {
        this.armorType= ArmorType.LeatherArmor;
    }

    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getBaseStrength() {
        return baseStrength;
    }

    @Override
    public int getBaseDexterity() {
        return baseDexterity;
    }

    @Override
    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    @Override
    public int getScaleHealthBy() {
        return scaleHealthBy;
    }

    @Override
    public int getScaleStrengthBy() {
        return scaleStrengthBy;
    }

    @Override
    public int getScaleDexterityBy() {
        return scaleDexterityBy;
    }

    @Override
    public int getScaleIntelligenceBy() {
        return scaleIntelligenceBy;
    }



}
