package armor;

public class Legs implements SlotArmor {

    // Factor is 0.6
    @Override
    public double getFactor() {
        return 0.6;
    }
}
