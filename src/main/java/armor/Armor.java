package armor;

import hero.GetBaseScales;
import hero.GetBaseStats;
import hero.levelUp.LevelUpStats;

public  abstract class Armor implements LevelUpStats, GetBaseStats, GetBaseScales {
    public Armor(int lvl, SlotArmor slotArmor, String name) {

            this.slotArmor=slotArmor;
            this.name=name;
            setArmorType();

            setArmorLevel(lvl);
        }


    //Member variables
    public SlotArmor slotArmor;
    public int lvl;
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;
    public ArmorType armorType;
    public String name;

    //getter and setters
    public int getArmorLevel() {
        return lvl;
    }

    public void setArmorLevel(int lvl) {
        this.lvl = lvl;
        levelUpAll(lvl);
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public String getName() {
        return name;
    }

    //Has to be set in subclass
    public abstract void setArmorType();

    //level up all attributes
    @Override
    public void levelUpAll(int toLvl){
        levelUpHealth(toLvl);
        levelUpStrength(toLvl);
        levelUpDexterity(toLvl);
        levelUpIntelligence(toLvl);
    }

    /* Methods to levelup stats to a certain lvl, calculated by scaling and base stats which is forced to be set from the
    getBase and getscales interfaces, these are set in the subclasses*/

    @Override
    public void levelUpHealth(int toLvl) {
        int statFormula= (int) ((toLvl*getScaleHealthBy()+getBaseHealth())* slotArmor.getFactor());
        setHealth(statFormula);
    }

    @Override
    public void levelUpStrength(int toLvl) {
        int statFormula= (int) ((toLvl*getScaleStrengthBy()+getBaseStrength())* slotArmor.getFactor());
        setStrength(statFormula);
    }

    @Override
    public void levelUpDexterity(int toLvl) {
        int statFormula= (int) ((toLvl*getScaleDexterityBy()+getBaseDexterity())* slotArmor.getFactor());
        setDexterity(statFormula);
    }

    @Override
    public void levelUpIntelligence(int toLvl) {
        int statFormula= (int) ((toLvl*getScaleIntelligenceBy()+getBaseIntelligence())* slotArmor.getFactor());
        setIntelligence(statFormula);
    }

    //return names of slots f.ex "Legs"
    public String getBonusSlot(){
        return slotArmor.getClass().getSimpleName();
    }




}
