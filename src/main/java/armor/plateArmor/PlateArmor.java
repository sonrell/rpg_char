package armor.plateArmor;

import armor.Armor;
import armor.ArmorType;
import armor.SlotArmor;

public class PlateArmor extends Armor {

    //Static properties for Plate
    static final int baseHealth=30;
    static final int baseStrength=3;
    static final int baseDexterity=1;
    static final int baseIntelligence=0;

    static final int scaleHealthBy=12;
    static final int scaleStrengthBy=2;
    static final int scaleDexterityBy=1;
    static final int scaleIntelligenceBy=0;

    public PlateArmor(int lvl, SlotArmor slotArmor, String name) {
        super(lvl, slotArmor, name);
    }
    //Setters and getters from interface and super
    @Override
    public void setArmorType() {
        this.armorType= ArmorType.PlateArmor;
    }


    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getBaseStrength() {
        return baseStrength;
    }

    @Override
    public int getBaseDexterity() {
        return baseDexterity;
    }

    @Override
    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    @Override
    public int getScaleHealthBy() {
        return scaleHealthBy;
    }

    @Override
    public int getScaleStrengthBy() {
        return scaleStrengthBy;
    }

    @Override
    public int getScaleDexterityBy() {
        return scaleDexterityBy;
    }

    @Override
    public int getScaleIntelligenceBy() {
        return scaleIntelligenceBy;
    }


}
