package armor;

public enum ArmorType {
    ClothArmor,
    LeatherArmor,
    PlateArmor
}
