package armor;

public class Head implements SlotArmor {

    //Factor to multiply stats by 0.8
    @Override
    public double getFactor() {
        return 0.8;
    }
}
