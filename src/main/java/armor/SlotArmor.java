package armor;

public interface SlotArmor {
//Strategy pattern to set a factor which base-stats will be multiplied by
    double getFactor();
}
