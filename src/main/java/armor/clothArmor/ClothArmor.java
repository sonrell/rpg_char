package armor.clothArmor;

import armor.Armor;
import armor.ArmorType;
import armor.SlotArmor;

public class ClothArmor extends Armor {

    // Static properties for Cloth
    static final int baseHealth=10;
    static final int baseStrength=0;
    static final int baseDexterity=1;
    static final int baseIntelligence=3;

    static final int scaleHealthBy=5;
    static final int scaleStrengthBy=0;
    static final int scaleDexterityBy=1;
    static final int scaleIntelligenceBy=2;

    public ClothArmor(int lvl, SlotArmor slotArmor, String name) {
        super(lvl, slotArmor, name);
    }

    // Overridden getter and setters  from super and interface

    @Override
    public void setArmorType() {
        this.armorType= ArmorType.ClothArmor;
    }

    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getBaseStrength() {
        return baseStrength;
    }

    @Override
    public int getBaseDexterity() {
        return baseDexterity;
    }

    @Override
    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    @Override
    public int getScaleHealthBy() {
        return scaleHealthBy;
    }

    @Override
    public int getScaleStrengthBy() {
        return scaleStrengthBy;
    }

    @Override
    public int getScaleDexterityBy() {
        return scaleDexterityBy;
    }

    @Override
    public int getScaleIntelligenceBy() {
        return scaleIntelligenceBy;
    }




}
