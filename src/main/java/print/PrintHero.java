package print;

import hero.Hero;



public class PrintHero {

    public static void printHero(Hero hero){
        System.out.printf("%s details:%nHP: %d %n", hero.getHeroType(),hero.getTotalHealth());
        System.out.printf("Str: %d %n", hero.getTotalStrength());
        System.out.printf("Dex: %d %n", hero.getTotalDexterity());
        System.out.printf("Int: %d %n", hero.getTotalIntelligence());
        System.out.printf("Lvl: %d %n", hero.getLvl());
        System.out.printf("XP: %d %n", hero.getXp());
        System.out.printf("XP to next: %d %n", hero.returnXptoNextLevel());
        System.out.printf("Attacking damage: %d%n%n", hero.getTotalDamageDealt());


    }
}
