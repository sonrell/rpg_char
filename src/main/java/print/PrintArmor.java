package print;

import armor.Armor;




public class PrintArmor {

    public static void printArmor(Armor armor){
        System.out.printf("Item stats for %s: %n", armor.getName());
        System.out.printf("Slot: %s %n",armor.getBonusSlot());
        System.out.printf("Armor type: %s %n",armor.getArmorType());
        System.out.printf("Armor Level: %d %n", armor.getArmorLevel());
        System.out.printf("Bonus HP: %d %n", armor.getHealth());
        System.out.printf("Bonus Str: %d %n", armor.getStrength());
        System.out.printf("Bonus Dex: %d %n", armor.getDexterity());
        System.out.printf("Bonus Int: %d %n%n", armor.getIntelligence());

    }
}
