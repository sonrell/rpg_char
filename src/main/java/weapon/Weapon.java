package weapon;

import hero.Hero;

public  abstract class Weapon {

    //Member variables
    public WeaponType weaponType;
    public int damageDealt;
    public int lvl;
    String name;


    public Weapon(int toLvl , String name) {
        levelWeapon(toLvl);
        setWeaponType();
        this.name=name;

    }

    //Setters and getters
    public int getDamageDealt() {
        return damageDealt;
    }
    public void setDamageDealt(int damageDealt) {
        this.damageDealt = damageDealt;
    }
    public void setLvl(int lvl) {
        this.lvl = lvl;
    }
    public String getName() {
        return name;
    }
    public WeaponType getWeaponType() {
        return weaponType;
    }
    public int getLvl() {
        return lvl;
    }
    //Setters and getters which has to be set in subClasses
    public abstract int getAddedDamageWhenEquipped(Hero hero);
    public abstract void setWeaponType();
    public abstract int getBaseDamage();
    public abstract int getDamageScaleBy();





    //Upgrade a weapon to the lvl requested
    public void levelWeapon(int toLvl){
        setDamageDealt(getBaseDamage()+getDamageScaleBy()*toLvl);
        setLvl(toLvl);

    }
}
