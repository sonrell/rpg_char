package weapon.meleeWeapon;

import hero.Hero;
import weapon.Weapon;
import weapon.WeaponType;

public class MeleeWeapon extends Weapon {
    //Static variables specific to Melee
    static final int   baseDamage=15;
    static final int  damageScaleBy=2;

    public MeleeWeapon(int toLvl, String name) {
        super(toLvl, name);
    }

    //Increases with hero's strength
    @Override
    public int getAddedDamageWhenEquipped(Hero hero) {
        return (int) (hero.getTotalStrength()*1.5);
    }

    //Set to Melee weapon, base damage and scale each lvl by.

    @Override
    public void setWeaponType() {
        this.weaponType= WeaponType.Melee;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;

    }

    @Override
    public int getDamageScaleBy() {
        return damageScaleBy;
    }
}
