package weapon;

public enum WeaponType {
    Melee,
    Ranged,
    Magic
}
