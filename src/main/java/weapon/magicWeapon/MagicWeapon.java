package weapon.magicWeapon;

import hero.Hero;
import weapon.Weapon;
import weapon.WeaponType;

public class MagicWeapon extends Weapon {
    //Static variables specific to Magic
     static final int   baseDamage=25;
     static final int  damageScaleBy=2;


    public MagicWeapon(int toLvl, String name) {
        super(toLvl, name);
    }


    //Damage added based on hero's intelligence
    @Override
    public int getAddedDamageWhenEquipped(Hero hero) {
        return hero.getTotalIntelligence()*3;
    }


    //Set to Mage weapon, base damage and scale each lvl by.
    @Override
    public void setWeaponType() {
        this.weaponType= WeaponType.Magic;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;

    }

    @Override
    public int getDamageScaleBy() {
        return damageScaleBy;
    }


}
