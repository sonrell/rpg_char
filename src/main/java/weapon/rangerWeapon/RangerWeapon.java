package weapon.rangerWeapon;

import hero.Hero;
import weapon.Weapon;
import weapon.WeaponType;

public class RangerWeapon extends Weapon {
    //Static variables specific to Ranger Weapon
    static final int   baseDamage=5;
    static final int  damageScaleBy=3;

    public RangerWeapon(int toLvl, String name) {
        super(toLvl, name);
    }

    //Increases with heros dexterity
    @Override
    public int getAddedDamageWhenEquipped(Hero hero) {
        return hero.getTotalDexterity()*2;
    }

    //Set to Ranger weapon, base damage and scale each lvl by.

    @Override
    public void setWeaponType() {
        this.weaponType= WeaponType.Ranged;
    }

    @Override
    public int getBaseDamage() {
        return baseDamage;

    }

    @Override
    public int getDamageScaleBy() {
        return damageScaleBy;
    }
}
