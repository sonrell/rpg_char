package hero.mage;

import hero.Hero;
import hero.HeroType;

public class Mage extends Hero {

    //Static properties to Mage
    static final int baseHealth=85;
    static final int baseStrength=1;
    static final int baseDexterity=1;
    static final int baseIntelligence=5;

    static final int scaleHealthBy=15;
    static final int scaleStrengthBy=1;
    static final int scaleDexterityBy=2;
    static final int scaleIntelligenceBy=5;

    public Mage(int lvl) {
        super(lvl);
    }
    //Overridden methods from super and interface which returns base and scaleby static variables
    @Override
    public void setHeroType() {
        this.hero=HeroType.Mage;
    }

    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getBaseStrength() {
        return baseStrength;
    }

    @Override
    public int getBaseDexterity() {
        return baseDexterity;
    }

    @Override
    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    @Override
    public int getScaleHealthBy() {
        return scaleHealthBy;
    }

    @Override
    public int getScaleStrengthBy() {
        return scaleStrengthBy;
    }

    @Override
    public int getScaleDexterityBy() {
        return scaleDexterityBy;
    }

    @Override
    public int getScaleIntelligenceBy() {
        return scaleIntelligenceBy;
    }


}
