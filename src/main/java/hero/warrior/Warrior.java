package hero.warrior;

import hero.Hero;
import hero.HeroType;

public class Warrior extends Hero {


    static final int baseHealth=120;
    static final int baseStrength=5;
    static final int baseDexterity=1;
    static final int baseIntelligence=0;

    static final int scaleHealthBy=30;
    static final int scaleStrengthBy=5;
    static final int scaleDexterityBy=2;
    static final int scaleIntelligenceBy=1;

    public Warrior(int lvl) {
        super(lvl);
    }

    //Overridden methods from super and interface which returns base and scaleby static variables

    @Override
    public void setHeroType() {
        this.hero= HeroType.Warrior;
    }
    @Override
    public int getBaseHealth() {
        return baseHealth;
    }

    @Override
    public int getBaseStrength() {
        return baseStrength;
    }

    @Override
    public int getBaseDexterity() {
        return baseDexterity;
    }

    @Override
    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    @Override
    public int getScaleHealthBy() {
        return scaleHealthBy;
    }

    @Override
    public int getScaleStrengthBy() {
        return scaleStrengthBy;
    }

    @Override
    public int getScaleDexterityBy() {
        return scaleDexterityBy;
    }

    @Override
    public int getScaleIntelligenceBy() {
        return scaleIntelligenceBy;
    }

}
