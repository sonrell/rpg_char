package hero;

public interface GetBaseStats {

     //Interface to force getters of baseStats

     int  getBaseHealth ();
     int  getBaseStrength ();
     int  getBaseDexterity ();
     int  getBaseIntelligence ();


}
