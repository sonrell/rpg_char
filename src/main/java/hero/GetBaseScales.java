package hero;

public interface GetBaseScales {

    //interface to force Scaling factor per level
    int  getScaleHealthBy ();
    int  getScaleStrengthBy ();
    int  getScaleDexterityBy ();
    int  getScaleIntelligenceBy ();


}
