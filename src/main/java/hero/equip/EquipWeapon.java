package hero.equip;

import armor.Armor;
import hero.Hero;
import weapon.Weapon;

public interface EquipWeapon {

    //Has to set totalDamagedealt in class which implements this interface
    public void setTotalDamageDealt(int toCalc);


    //Updates the totalDamagedealt to the hero to the  weapons damage + the bonus which is set in weapon subclass
    public static void equipWeapon(Weapon weapon, Hero hero){
        if (hero.getLvl()>=weapon.getLvl()){
            hero.setTotalDamageDealt(weapon.getAddedDamageWhenEquipped(hero)+weapon.getDamageDealt());
        }else{
            System.out.println("The hero's lvl is not high enough to carry this weapon");
        }

    }
    public static void removeWeapon(Hero hero){
        hero.setTotalDamageDealt(Hero.damageDealtNoWeapon);
    }


}
