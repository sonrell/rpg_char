package hero.equip;

import armor.Armor;
import hero.Hero;

public interface EquipArmor {


// Methods to be set in which ever class which implements this class
    void setAddedHealth(int toCalc);
    void setAddedStrength(int toCalc);
    void setAddedDexterity(int toCalc);
    void setAddedIntelligence(int toCalc);

    //equipping armor which updates addedStats in Hero, also checks the lvl of hero is high enough
        static void equipArmor(Armor armor, Hero hero){
            if (hero.getLvl()>=armor.lvl) {
                hero.setAddedHealth(armor.health );
                hero.setAddedStrength(armor.strength );
                hero.setAddedDexterity(armor.dexterity );
                hero.setAddedIntelligence(armor.intelligence );
            }else{
                System.out.printf("The hero's lvl is not high enough to use this armor");
            }
    }
        static void removeArmor(Hero hero){
            hero.setAllAddedEqualToZero();
        }


}
