package hero;

public enum HeroType {
    Warrior,
    Mage,
    Ranger
}
