package hero.levelUp;

public interface LevelUpStats {
    //Interface to  levelup the stats
    void levelUpHealth(int toLvl);
    void levelUpStrength(int toLvl);
    void levelUpDexterity(int toLvl);
    void levelUpIntelligence(int toLvl);
    void levelUpAll(int toLvl);
}
