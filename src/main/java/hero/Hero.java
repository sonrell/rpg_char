package hero;

import hero.equip.EquipArmor;
import hero.equip.EquipWeapon;
import hero.levelUp.LevelUpStats;



public abstract class Hero implements LevelUpStats,GetBaseStats,GetBaseScales, EquipArmor, EquipWeapon {
    //Member variables:
    public HeroType hero;

    //added stats when something is equipped
    public int addedStrength;
    public int addedDexterity;
    public int addedIntelligence;
    public int addedDamageDealt;
    public int addedHealth;
    public int totalDamageDealt;

    //baseStats
    public int lvl;
    public int xp;
    public int health;
    public int strength;
    public int dexterity;
    public int intelligence;
    public int damageDealt;
    public static final int damageDealtNoWeapon=0;



    //Constructor
    public Hero(int toLvl) {
        setLvl(toLvl);
        setHeroType();
        setDamageDealt(damageDealtNoWeapon);
        setAllAddedEqualToZero();

    }

    public void setAllAddedEqualToZero() {
        this.addedHealth = 0;
        this.addedStrength = 0;
        this.addedDexterity = 0;
        this.addedIntelligence = 0;

    }


    //Getters for all Member Variables:
    public HeroType getHeroType() {
        return hero;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getLvl() {
        return lvl;
    }

    //Modified to when a lvl is set a start xp for that lvl is set to
    public void setLvl(int toLvl) {
        this.lvl = toLvl;
        levelUpAll(toLvl);
        if (this.xp<returnStartXpByLevel(toLvl)||this.xp>returnStartXpByLevel(toLvl+1)){
            this.xp=(returnStartXpByLevel(toLvl));
        }


    }

    public int getXp() {
        return xp;
    }
    //Modified to set to the appropriate lvl which corresponds to the xp
    public void setXp(int xp) {
        this.xp = xp;
       while (this.xp>=returnStartXpByLevel(this.lvl+1)){
            this.lvl+=1;
        }
       while (this.xp<returnStartXpByLevel(this.lvl)){
           this.lvl-=1;
       }
       levelUpAll(this.lvl);
    }
    //HeroType is set in subclass
    public abstract void setHeroType();
    public void setDamageDealt(int damageDealt) {
        this.damageDealt = damageDealt;
    }
    public int getAddedDamageDealt() {
        return addedDamageDealt;
    }

    public void setAddedDamageDealt(int addedDamageDealt) {
        this.addedDamageDealt = addedDamageDealt;
    }

    //Start xp for each lvl is returned
    private int returnStartXpByLevel(int toLvl) {
        double calc=0;
        if (toLvl==1){
            return 0;
        }
        for (int i = 2; i <=toLvl ; i++) {
            calc+=100*Math.pow(1.1,i-2);
        }

        return (int)calc;

    }

    //Returns how much xp needed to lvl up to next lvl
    public int returnXptoNextLevel() {
        return returnStartXpByLevel(this.lvl + 1) - getXp();
    }

    @Override
    //Level upp all base stats from interface
    public void levelUpAll(int toLvl) {
        levelUpHealth(toLvl);
        levelUpStrength(toLvl);
        levelUpDexterity(toLvl);
        levelUpIntelligence(toLvl);
    }
    //level up each base stats, forced by interface
    @Override
    public void levelUpHealth(int toLvl) {
        setHealth(toLvl * getScaleHealthBy() + getBaseHealth());
    }

    @Override
    public void levelUpStrength(int toLvl) {
        setStrength(toLvl * getScaleStrengthBy() + getBaseStrength());
    }

    @Override
    public void levelUpDexterity(int toLvl) {
        setDexterity(toLvl * getScaleDexterityBy() + getBaseDexterity());
    }

    @Override
    public void levelUpIntelligence(int toLvl) {
        setIntelligence(toLvl * getScaleIntelligenceBy() + getBaseIntelligence());
    }

    //setters and getter for added a<attributes because of equipment
    public int getAddedStrength() {
        return addedStrength;
    }

    @Override
    public void setAddedStrength(int addedStrength) {
        this.addedStrength = addedStrength;
    }

    public int getAddedDexterity() {
        return addedDexterity;
    }

    @Override
    public void setAddedDexterity(int addedDexterity) {
        this.addedDexterity = addedDexterity;
    }

    public int getAddedIntelligence() {
        return addedIntelligence;
    }

    @Override
    public void setAddedIntelligence(int addedIntelligence) {
        this.addedIntelligence = addedIntelligence;
    }



    public int getAddedHealth() {
        return addedHealth;
    }

    @Override
    public void setAddedHealth(int addedHealth) {
        this.addedHealth = addedHealth;
    }

    public int getTotalDamageDealt() {
        return totalDamageDealt;
    }

    @Override
    public void setTotalDamageDealt(int totalDamageDealt) {
        this.totalDamageDealt = totalDamageDealt;
    }


    //returns base + added stats because of equipment
    public int getTotalHealth(){
        return getHealth()+getAddedHealth();
    }
    public int getTotalStrength(){
        return getStrength()+getAddedStrength();
    }
    public int getTotalDexterity(){
        return getDexterity()+getAddedDexterity();
    }
    public int getTotalIntelligence(){
        return getIntelligence()+getAddedIntelligence();
    }

}

